# Secrets from TV shows

## Dades generals:
<ul>
<li> Nom del projecte: Secrets from TV shows </li>
<li>Temàtica de la web: Curiositats i secrets de diferents sèries de televisió (Netflix, HBO, etc.).</li>
<li>Descripció de les pàgines: La web tindrà una pàgina principal amb l'explicació d'aquesta pàgina web i després tindria diferents pàgines per a cada sèrie.</li>
</ul>

## Estructura de la pàgina web:
<ul>
<li>Web</li>
    <ul>
        <li>Serie1</li>
        <li>Serie2</li>
        <li>Serie3</li>
    </ul>
</ul>

##### Diana Gallardo Pérez